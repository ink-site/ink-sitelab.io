# Getting help

**If you have a general query about INK**, or want to discuss anything with us, come and [chat to us in our Mattermost channel](https://mattermost.coko.foundation/coko/channels/ink).

**Bug reports and feature requests** belong in the issues of the relevant repository:
  - [`ink-api`](https://gitlab.coko.foundation/ink/ink-api/issues)
  - [`ink-client`](https://gitlab.coko.foundation/ink/ink-client/issues)

**If you're not sure where an issue belongs** please ask in the chat room and we'll be happy to help.
