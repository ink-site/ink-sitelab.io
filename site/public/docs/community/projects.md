# Projects using INK

## Editoria

[Editoria](https://gitlab.coko.foundation/editoria/editoria) is a book editing platform built using [PubSweet](http://pubsweet.org) in collaboration with the University of California Press by Coko Foundation, and in particular by the Editoria team based in Athens.

INK is used in Editoria to ingest author-provided book manuscripts. Editoria provides its own interface for this, while INK seamlessly performs the necessary conversions and checks in the background.

Editoria will soon be deployed for book production at UCP.

- [Source code repository](https://gitlab.coko.foundation/editoria/editoria)
- [Issue tracker](https://gitlab.coko.foundation/editoria/editoria/issues)
- [Discussion room](https://mattermost.coko.foundation/coko/channels/editoria)

## xpub

[*xpub*](https://gitlab.coko.foundation/xpub/xpub) is a journal platform under development by Coko Foundation in collaboration with eLife and Hindawi. It uses the [PubSweet toolkit](http://pubsweet.org).

INK is used in xpub via a custom interface to process user uploaded files including manuscripts and assocated assets.

- [Source code repository](https://gitlab.coko.foundation/xpub/xpub)
- [Issue tracker](https://gitlab.coko.foundation/xpub/xpub/issues)
