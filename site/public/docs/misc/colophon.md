## Colophon

Ink has been developed in New Zealand by Charlie Ablett.

The INK documentation is built with the [Harp](http://harpjs.com) static site generator.

The body text is set in **Spectral**, designed by [Production type](https://www.productiontype.com/) for Google and available on [Google fonts](https://fonts.google.com/specimen/Spectral) while the headings are composed in **Fira Sans** and all the coding in **Fira Mono**, designed by [Erik Spiekermann](https://www.edenspiekermann.com/), both available from [Mozilla](http://mozilla.github.io/Fira/).

The INK logo was made by Henrik Van Leeuwenn from the [Van Leeuwenn brothers](http://www.vanleeuwenbrothers.com/).

The syntax highlighting of the website is based on [Prism.js](http://prismjs.com) which was made by [Lea Verou](http://lea.verou.me/).

The website was designed by Julien Taquet and the content was written by Charlie Ablett and Rik Smith-Unna.

And the whole INK universe is made possible thanks to the work of the [Collaborative Knowledge Foundation](http://www.coko.foundation).
