# Installing INK

> NOTE: This is a technical guide for systems administrators.

The quickest and most maintainable way to get a production INK instance up and running is using [Docker](https://www.docker.com/). INK is provided as a dockerized service, and can be run along with all dependency services using `docker compose`.

This guide shows you how to do that, with commands designed for a **fresh** ubuntu LTS (16.04) installation. If you need to set up INK on a different system and need support, please [get in contact](/community/help) and we'll be happy to help.

## Pre-requisites

### Ruby

You'll need ruby, preferably via a ruby version manager like `rvm`. Here we set up [`rvm`](http://rvm.io/) and install the required ruby version (`v2.2.3`).

On ubuntu:

```bash
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
\curl -sSL https://get.rvm.io | bash -s stable --ruby=2.2.3
source /usr/local/rvm/scripts/rvm
```

To confirm the `rvm` and the correct `ruby` version are installed, run:

```bash
rvm ls
```

You should see:

```bash
rvm rubies

=* ruby-2.2.3 [ x86_64 ]

# => - current
# =* - current && default
#  * - default
```

### Ruby tools

Bundler is used to manage ruby dependencies for INK. To install bundler, we use the ruby `gem` package manager:

```bash
gem install bundler
```

### Docker tools

You'll need to install `docker` and `docker-compose`.

On ubuntu:

```bash
sudo apt-get remove docker docker-engine docker.io
sudo apt-get update
sudo apt-get -y install \
    linux-image-extra-$(uname -r) \
    linux-image-extra-virtual
sudo apt-get update
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce
sudo curl -L https://github.com/docker/compose/releases/download/1.16.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

## INK API

### Getting the code

INK is downloaded from its [Gitlab repository](https://gitlab.coko.foundation/INK/ink-api).

We will do this using `git` (but you can also download and unpack the repo in [zip](https://gitlab.coko.foundation/INK/ink-api/repository/master/archive.zip) or [tar.gz](https://gitlab.coko.foundation/INK/ink-api/repository/master/archive.tar.gz) formats).

To clone the repo:

```bash
git clone -b production-docker https://gitlab.coko.foundation/blahah/ink-api
```

then `cd ink-api` to set the repository as your working directory.

### Configuration

We need to configure the various services that INK will depend on before we run them. We do this by creating a `.env` file containing environment variables that will be added into the environment of each docker container when it runs.

To generate a `.env` file, you run the generator script (`./bin/create-env`) and provide it with:

- the IP address or hostname of the server
- an email address for the server admin

E.g.:

```bash
./bin/create-env 123.34.45.56 me@mydomain.com
```

The script populates the `.env` file with securely generated secret keys for the various services INK relies upon, and adds all necessary config options.

### Running INK API

We use docker and docker-compose to build and run the INK containers:

```bash
docker volume create --name gems
./bin/docker
```

Now you should be inside the running INK docker container. We now install the ruby dependencies and set up the database:

```bash
bundle
rake db:create
rake db:schema:load
rake db:seed
```

Keep this terminal window open

## INK-client

Before we run the server, we first need to download the client-side code and generate a browser bundle using the same settings as we used for the server.

**Note: Do all the following steps in a new terminal window**

### Getting the code

As with `ink-api`, we will download the `ink-client` code from its [Gitlab repository](https://gitlab.coko.foundation/INK/ink-client).

We will do this using `git` (but you can also download and unpack the repo in [zip](https://gitlab.coko.foundation/INK/ink-client/repository/master/archive.zip) or [tar.gz](https://gitlab.coko.foundation/INK/ink-client/repository/master/archive.tar.gz) formats).

**Note: you should place the `ink-client` repo in the same directory that contains `ink-api` in order for all the following commands to work**

To clone the repo:

```bash
git clone https://gitlab.coko.foundation/INK/ink-client.git
```

then `cd ink-client` to set the repository as your working directory.

### Configuration

We need to set the IP/host of the server, and set up the secure communication between client and server.

To do this we edit two files:

1. `./settings.js`
  - replace `http://ink-api.coko.foundation` with your own IP or hostname, e.g. `http://123.23.34.45`
2. `./config/slanger-production.yml`
  - replace all IP addresses with your own IP or hostname
  - replace the `app_key` (set to `44332211ffeeddccbbaa` by default) with the value of `SLANGER_KEY` from the `ink-api` `.env`

### Building the client bundle

To generate all the necessary files, we run

```bash
npm run build
```

This creates all the bundle files in `./dist`, and starts a server with them. Kill the server using `ctrl C` to return to a command-line prompt.

### Installing the client bundle

Now we copy all the client files to where the server can find them.

Assuming you placed `ink-client` in the same directory as `ink-api`:

```bash
cp -R src/fonts src/images dist/
cp -R dist/* ../ink-api/public/
```

All the files should now be in place to allow us to run the server and client.

## Running the app

Return to the `ink-api` terminal window and run the app:

```bash
./bin/server
```

### Logging in

Now you can go to `http://YOUR_HOSTNAME_OR_IP:3000` and log in. The default account is:

- username: `inkdemo@example.com`
- password: `password`
