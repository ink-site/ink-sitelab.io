# Creating an INK step

> NOTE: This is a technical guide for developers.

Creating an INK step is usually a straightforward process. Most INK steps take an existing tool and wrap it in some ruby code that runs the tool and tells INK about it. The wrapping code is mostly generated for you using the generator command-line tool, so you can focus on the functionality.

This guide helps you get started by showing how to generate a template step and fill it in. The majority of the guide is contained within the INK step itself. Here, we show how to install the required dependencies (`ruby` and `bundler`), install the step generator, and run it to create an example step. From there, you can customise it by following the instructions in the README of the generated step directory.

The instructions below assume you are using the latest Ubuntu LTS release as your operating system, but should translate easily to other linux systems and to macOS.

## Pre-requisites

### Ruby

You'll need ruby, preferably via a ruby version manager like `rvm`. Here we set up [`rvm`](http://rvm.io/) and install the required ruby version (`v2.2.3`).

On ubuntu:

```bash
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
\curl -sSL https://get.rvm.io | bash -s stable --ruby=2.2.3
source /usr/local/rvm/scripts/rvm
```

To confirm the `rvm` and the correct `ruby` version are installed, run:

```bash
rvm ls
```

You should see:

```bash
rvm rubies

=* ruby-2.2.3 [ x86_64 ]

# => - current
# =* - current && default
#  * - default
```

### Ruby tools

Bundler is used to manage ruby dependencies for INK. To install bundler, we use the ruby `gem` package manager:

```bash
gem install bundler
```

## Installing the generator

The INK step generator is a command-line tool (for macOS and linux). It uses the ruby [ore](https://github.com/ruby-ore/ore) generator.

There are two steps involved in installation:

1. installing `ore`
2. adding the [`ink-step-gem`](https://gitlab.coko.foundation/blahah/ink-step-gem.git) template to ore

```bash
gem install ore
ore install https://gitlab.coko.foundation/blahah/ink-step-gem.git
```

## Generating an INK step

To generate an INK step you need to prepare some basic metadata and then simply run the generator command-line tool, `mine`.

Let's say you want to call the step `mystep`. You would run the following command:

```bash
mine mystep --ink-step-gem \
 --name="my-step" \
 --namespace="MyStep" \
 --summary="an example INK step" \
 --description="an example INK step generated using ink-step-gem" \
 --author="My Name" \
 --email="me@email.com" \
 --website="http://myinkstep.com"
```

> Note: make sure to separate words with hyphens in the `--name` argument, and use SnakeCase in the `--namespace` argument

You can optionally leave out the website and email.

Running this command will print some progress information to the command line. Once it exits, there will be a new directory in the current working directory containing the INK step that was generated. For example if you used the exact command above, there would be a new directory called `mystep`.

## Customising your INK step

To add your own functionality to the newly generated INK step, follow the instructions in the `README.md` of the directory that was created.
