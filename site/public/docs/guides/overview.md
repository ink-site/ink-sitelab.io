# What INK can do

This is a non-technical overview of what INK can do. It introduces the central concepts of the system and explains the ways it can be deployed and adapted to meet the needs of an organisation.

## Recipes describe pipelines

The central concept of INK is the **recipe**. An INK recipe represents a specific reusable document processing workflow.

<center><img src="/images/ink_recipes.png" class="diagram"></center>

Recipes can be copied, modified, and shared between users of a system.

## Steps describe specific tasks

A recipe is composed of one or more **steps** - each of which is a specific task that works on a document or its associated assets (like images, video and audio). For example, a step might do one of the following:

- convert a document or its assets from one format to another
- validate the syntax or content of a document
- annotate or enhance a document

<center><img src="/images/ink_steps.png" class="diagram"></center>

Like recipes, steps can be shared between users of a system. Steps can also be reused *between* INK systems. This means you can use INK steps already shared on the internet if they meet your needs. If you need a step that doesn't exist, they are [easy to create](/docs/guides/creating_steps.html).

## Documents are processed by running recipes

To use a recipe, you run it by providing one or more documents. INK then passes the input document(s) through each step in the recipe in turn. The output of each step becomes the input to the subsequent step. The final output is returned back to the user.

<center><img src="/images/ink_execution.png" class="diagram"></center>

## INK has a built-in user interface

INK includes a web-based user interface that is specialised for creating, managing and running recipes.

For any given recipe, users can upload documents to run the recipe any number of times. Execution of recipes can be monitored in real-time through the interface, and the results are available immediately to explore and download. Past work can be easily discovered and retrieved.

<center><img src="/images/ink_client.png" class="diagram"></center>

In addition to receiving the final output of a recipe, you can also retrieve the results of any step in a recipe. This allows manual quality assurance checking and problem-solving if the output is different than expected.

## INK can be integrated seamlessly with external platforms

It's not necessary to use the built-in web interface - INK can be used as a standalone service for document processing that integrates with existing platforms and user-facing systems. An external platform can send document processing tasks to an INK service and receive the results seamlessly. Platforms integrating with INK in this way can access all the same features that are available through the user interface.

<center><img src="/images/ink_api.png" class="diagram"></center>

The [PubSweet](http://pubsweet.org) toolkit for building publishing platforms provides several prebuilt components that integrate with INK. For example, it includes a document editor that can use INK in the background to convert user-uploaded documents before loading them into the editor.

## Find out more

To explore how your organisation could use INK please [get in touch](/docs/community/help.html).

If you'd like more detail about how INK works, you can find it in the [INK v1.0 guide](/images/INK_v1_guide.pdf).
