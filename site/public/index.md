# INK - Ingest 'N Konversion

## the free and open web-based job management service

**INK** is a free, open source tool for constructing and managing file conversion pipelines. It can also be used for content enrichment, entity extraction, conditional rendering, and more!

INK was developed to serve the needs of publishers, but works equally well in any domain. It is integrated into the [PubSweet](http://pubsweet.org) toolkit for building publishing workflows, and is in production use at University of California Press in their book editing platform, [Editoria](http://editoria.pub).

Want to learn more? Read the non-technical overview of [what INK can do](/docs/guides/overview.html).

To dive right in, learn how to [install INK](/docs/guides/installing.html) or [create an INK step](/docs/guides/creating_steps.html).
